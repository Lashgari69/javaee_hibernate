package org.myblog.example.model.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "users")
@Table(name = "users")
public class Users implements Serializable {
    @Id@GeneratedValue(strategy = GenerationType.AUTO)
    private Long usersId;
    @Column(columnDefinition = "NVARCHAR2(50)")
    private String username;
    @Column(columnDefinition = "NVARCHAR2(50)")
    private String email;
    @Column(columnDefinition = "NVARCHAR2(50)")
    private String password;
    @Column(columnDefinition = "NVARCHAR2(50)")
    private String roleName;
    @Version
    private Long recordVersion;

    public Users() {}

    public Users(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
    }

    public Users(String username, String email, String password, String roleName) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.roleName = roleName;
    }

    public Users(Long usersId, String username, String email, String password) {
        this.usersId = usersId;
        this.username = username;
        this.email = email;
        this.password = password;
    }

    public Users(Long usersId, String username, String email, String password, String roleName) {
        this.usersId = usersId;
        this.username = username;
        this.email = email;
        this.password = password;
        this.roleName =roleName;
    }

    public Users(Long usersId, String username, String email, String password, String roleName, Long recordVersion) {
        this.usersId = usersId;
        this.username = username;
        this.email = email;
        this.password = password;
        this.roleName = roleName;
        this.recordVersion = recordVersion;
    }

    public Long getUsersId() {
        return usersId;
    }

    public Users setUsersId(Long usersId) {
        this.usersId = usersId;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public Users setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Users setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public Users setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getRoleName() {
        return roleName;
    }

    public Users setRoleName(String roleName) {
        this.roleName = roleName;
        return this;
    }

    public Long getRecordVersion() {
        return recordVersion;
    }

    public Users setRecordVersion(Long recordVersion) {
        this.recordVersion = recordVersion;
        return this;
    }
}