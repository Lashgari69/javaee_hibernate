package org.myblog.example.model.service;

import org.myblog.example.common.InvalidUserNameOrPassword;
import org.myblog.example.common.JPA;
import org.myblog.example.model.entity.Users;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;

public class UsersService {
    public static void save(Users users)
    {
        EntityManager entityManager = JPA.getEntityManager();
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();
        entityManager.persist(users);
        entityTransaction.commit();
        entityManager.close();
    }

    public static void update(Users users)
    {
        EntityManager entityManager = JPA.getEntityManager();
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();
        Users users1 = entityManager.find(Users.class,users.getUsersId());
        users1.setUsername(users.getUsername()).setEmail(users.getEmail()).setPassword(users.getPassword());
        users1.setRecordVersion(1L);
        entityManager.persist(users1);
        entityTransaction.commit();
        entityManager.close();
    }

    public static void remove(Long id)
    {
        EntityManager entityManager = JPA.getEntityManager();
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();

        Users user = entityManager.find(Users.class,id);
        entityManager.remove(user);

        entityTransaction.commit();
        entityManager.close();
    }

    public static List<Users> findAll(){
        EntityManager entityManager = JPA.getEntityManager();
        Query query = entityManager.createQuery("select o from users o");
//        query.setFirstResult(0);
//        query.setMaxResults(5);
        List<Users> usersList = query.getResultList();
        entityManager.close();
        return usersList;
    }

    public static Users findOne(Users users) throws InvalidUserNameOrPassword {
        System.out.println(users.getEmail());
        System.out.println(users.getPassword());
        EntityManager entityManager = JPA.getEntityManager();
        Query query = entityManager.createQuery("select o from users o where o.email=:e and o.password=:p");
        query.setParameter("e", users.getEmail());
        query.setParameter("p", users.getPassword());
        Users users1 = (Users) query.getSingleResult();
        entityManager.close();
        if (users1.getRoleName().isEmpty()) {
            throw new InvalidUserNameOrPassword();
        } else {
            return users1;
        }
    }

}
