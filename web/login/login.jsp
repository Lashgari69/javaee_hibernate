<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <jsp:include page="/WEB-INF/header.jsp"/>
</head>
<body>
<jsp:include page="/WEB-INF/menu.jsp"/>

<div class="container">
  <form class="from-signin" action="/login" >
    <h2 class="form-signin-heading">Please login</h2>
    <label for="inputEmail" class="sr-only">Email</label>
    <input id="inputEmail" type="text" class="form-control" name="email" required autofocus/>
    <label for="inputPassword" class="sr-only">Password</label>
    <input id="inputPassword" type="password" class="form-control" name="password"/>
    <input type="submit" class="btn btn-lg btn-primary btn-block" value="LOGIN"/>
  </form>
</div>
</body>
</html>