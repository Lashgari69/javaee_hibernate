package org.myblog.example.common;

import java.sql.SQLException;

public class ExceptionWrapper {
    public static String getError(Exception e)
    {
        e.printStackTrace();
        if (e instanceof SQLException){
            return "101:DATABASE ERROR";
        } else if (e instanceof ArithmeticException){
            return "102:ARITHMATIC ERROR";
        } else if (e instanceof NumberFormatException){
            return "103:NOT VALID NUMBER";
        } else if (e instanceof ValidationException) {
            return "104:NOT VALID DATA";
        } else {
            return "200:CONNECT TO SUPPORT";
        }
    }
}
