package org.myblog.example.controller;

import org.hibernate.sql.Update;
import org.myblog.example.common.ExceptionWrapper;
import org.myblog.example.common.ValidationException;
import org.myblog.example.model.entity.Users;
import org.myblog.example.model.service.UsersService;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/")
public class UsersController extends HttpServlet {

    private UsersService usersService;
    private Users users;
    @Override
    public void init()
    {
        usersService = new UsersService();
        users = new Users();
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getServletPath();
        try {
            switch (action) {
                case "/register/save":
                    Save(req, resp);
                    break;
                case "/register/findAll":
                    FindAll(req, resp);
                    break;
                case "/register/update":
                    Update(req, resp);
                    break;
                case "/register/remove":
                    Remove(req, resp);
                    break;
                case "/login/":
                    Login(req, resp);
                    break;
                case "/logout":
                    Logout(req,resp);
                    break;
                default:
                    req.getRequestDispatcher("/register/index.jsp").forward(req, resp);
            }
        } catch (Exception e) {
            req.setAttribute("msg", ExceptionWrapper.getError(e));
            req.getRequestDispatcher("/error.jsp").forward(req, resp);
        }
    }

    private void Save(HttpServletRequest req, HttpServletResponse resp) throws ServletException,IOException{
        try{
            if (req.getParameter("username").equals("admin")){
                users.setUsername(req.getParameter("username")).setEmail(req.getParameter("email")).setPassword(req.getParameter("password")).setRoleName("admin");
            } else {
                users.setUsername(req.getParameter("username")).setEmail(req.getParameter("email")).setPassword(req.getParameter("password")).setRoleName("public");
            }
            if (users.getUsername().isEmpty()) {throw new ValidationException();}

            usersService.save(users);
            resp.sendRedirect("/register/findAll");

        }catch (Exception e){
            req.setAttribute("msg", ExceptionWrapper.getError(e));
            req.getRequestDispatcher("/error.jsp").forward(req, resp);
        }
    }

    private void FindAll(HttpServletRequest req, HttpServletResponse resp) throws ServletException,IOException {
        try {
            List<Users> usersList = usersService.findAll();
            req.setAttribute("list",usersList);
            req.getRequestDispatcher("/register/index.jsp").forward(req, resp);
        } catch (Exception e) {
            req.setAttribute("msg", ExceptionWrapper.getError(e));
            req.getRequestDispatcher("/error.jsp").forward(req, resp);
        }
    }

    private void Update(HttpServletRequest req, HttpServletResponse resp) throws ServletException,IOException{
        try {
            users.setUsersId(Long.parseLong(req.getParameter("id"))).setUsername(req.getParameter("username"))
                    .setEmail(req.getParameter("email")).setPassword(req.getParameter("password"));
            usersService.update(users);
            resp.sendRedirect("/register/findAll");
        } catch (Exception e) {
            req.setAttribute("msg", ExceptionWrapper.getError(e));
            req.getRequestDispatcher("/error.jsp").forward(req, resp);
        }

    }

    private void Remove(HttpServletRequest req, HttpServletResponse resp) throws ServletException,IOException {
        try {
            //        remove
//        UsersService.remove(37L);
            Long id = Long.parseLong(req.getParameter("id"));
            usersService.remove(id);
            resp.sendRedirect("/register/findAll");
        } catch (Exception e) {
            req.setAttribute("msg" , ExceptionWrapper.getError(e));
            req.getRequestDispatcher("/error.jsp").forward(req, resp);
        }
    }

    private void Login(HttpServletRequest req, HttpServletResponse resp) throws ServletException,IOException {
        try {
            users.setEmail(req.getParameter("email")).setPassword(req.getParameter("password"));
            Users users1 = usersService.findOne(users);
            req.getSession().setAttribute("users",users1);
            resp.sendRedirect("/" + users1.getRoleName() + "/index.jsp");
        } catch (Exception e) {
            req.setAttribute("msg" , ExceptionWrapper.getError(e));
            req.getRequestDispatcher("/error.jsp").forward(req, resp);
        }
    }

    private void Logout(HttpServletRequest req, HttpServletResponse resp) throws ServletException,IOException {
        try {
            req.getSession().invalidate();
            resp.sendRedirect("/login/login.jsp");
        } catch (Exception e) {
            req.setAttribute("msg", ExceptionWrapper.getError(e));
            req.getRequestDispatcher("/error.jsp").forward(req, resp);
        }
    }
}