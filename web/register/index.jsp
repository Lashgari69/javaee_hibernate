<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <jsp:include page="/WEB-INF/header.jsp"/>
</head>
<body>
<jsp:include page="/WEB-INF/menu.jsp"/>
<div class="container">
  <div class="panel panel-primary">
    <div class="panel-heading">REGISTER CRUD</div>
    <div class="panel-body">
      <form action="/register/save">
        <input type="text" class="form-control" name="username"/>
        <br/>
        <input type="text" class="form-control" name="email"/>
        <br/>
        <input type="text" class="form-control" name="password"/>
        <br/>
        <input type="submit" class="btn btn-info" style="width: 100%;" value="SAVE"/>
      </form>
      <table class="table table-hover table-striped" style="width: 100%">
        <tr>
          <td>ID</td>
          <td>NAME</td>
          <td>EMAIL</td>
          <td>PASSWORD</td>
          <td>OPER</td>
          <td>OPER</td>
        </tr>
        <c:forEach items="${requestScope.list}" var="register">
          <form action="/register/update">
            <tr>
              <td><input type="text" class="form-control" name="id" value="${register.usersId}" readonly/></td>
              <td><input type="text" class="form-control" name="username" value="${register.username}"/></td>
              <td><input type="text" class="form-control" name="email" value="${register.email}"/></td>
              <td><input type="text" class="form-control" name="password" value="${register.password}"/></td>
              <td><input type="submit" class="btn btn-info" style="width: 100%;" value="UPDATE"/></td>
              <td><input type="button" class="btn btn-danger" style="width: 100%;" value="REMOVE"
                         onclick="removePerson(${register.usersId})"/></td>
            </tr>
          </form>
        </c:forEach>
      </table>
    </div>
  </div>
</div>
<script>
  function removePerson(id) {
    window.location = '/register/remove?id=' + id;
  }
</script>
</body>
</html>
